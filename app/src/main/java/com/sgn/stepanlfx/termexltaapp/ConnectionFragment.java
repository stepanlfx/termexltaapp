package com.sgn.stepanlfx.termexltaapp;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.robotpajamas.blueteeth.BlueteethDevice;
import com.robotpajamas.blueteeth.BlueteethManager;

import butterknife.BindView;

public class ConnectionFragment extends Fragment {
    Context MainContext;

    ListView LDevices;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwipeRefresh;
    private DeviceScanListAdapter mDeviceAdapter;
    BlueteethDevice mBlueteethDevice;

    int DevNum=-1;

    private static final int REQ_BLUETOOTH_ENABLE = 1000;
    private static final int DEVICE_SCAN_MILLISECONDS = 10000;

    public interface OnConnectionListener {
        void onConnectionListener(BlueteethDevice sBlueteethDevice);
    }

    private OnConnectionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConnectionListener) {
            mListener = (OnConnectionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragment1DataListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ConView =  inflater.inflate(R.layout.connection, container, false);
        setRetainInstance(true);
        MainContext = getActivity();

        mSwipeRefresh = ConView.findViewById(R.id.swiperefresh);
        LDevices = ConView.findViewById(R.id.listViewD);

        // If BLE support isn't there, quit the app
        checkBluetoothSupport();

        mSwipeRefresh.setOnRefreshListener(this::startScanning);
        mDeviceAdapter = new DeviceScanListAdapter(getActivity());
        LDevices.setAdapter(mDeviceAdapter);

        LDevices.setOnItemClickListener((parent, itemClicked, position, id) -> {
            if (DevNum != position)
            {
                DevNum = position;
                LDevices.requestFocusFromTouch();
                LDevices.setSelection(position);
                LDevices.setSelector(R.color.colorSelector);
            }
            else
            {
                LDevices.clearFocus();
                DevNum = -1;
                LDevices.setSelector(R.color.colorAccent);
            }
            mBlueteethDevice = mDeviceAdapter.getItem(position);
            mListener.onConnectionListener(mBlueteethDevice);
            //LDevices.setBackgroundColor();
        });

        return ConView;
    }



    private void startScanning() {
        //проверяю наличие разрешения на определение локации
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //если разрешения нет, то запрашиваю
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }
        // Clear existing devices (assumes none are connected)
        mDeviceAdapter.clear();
        BlueteethManager.with(getActivity()).scanForPeripherals(DEVICE_SCAN_MILLISECONDS, bleDevices -> {
            mSwipeRefresh.setRefreshing(false);
            for (BlueteethDevice device : bleDevices) {
                if (!TextUtils.isEmpty(device.getBluetoothDevice().getName())) {
                    mDeviceAdapter.add(device);
                }
            }
        });
    }

    private void stopScanning() {
        // Update the button, and shut off the progress bar
        mSwipeRefresh.setRefreshing(false);
        BlueteethManager.with(getActivity()).stopScanForPeripherals();
    }

    private void checkBluetoothSupport() {
        // Check for BLE support - also checked from Android manifest.
        if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            exitApp("No BLE Support...");
        }

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            exitApp("No BLE Support...");
        }

        //noinspection ConstantConditions
        if (!btAdapter.isEnabled()) {
            enableBluetooth();
        }
    }

    private void exitApp(String reason) {
        // Something failed, exit the app and send a toast as to why
        Toast.makeText(MainContext.getApplicationContext(), reason, Toast.LENGTH_LONG).show();
        getActivity().onBackPressed();
    }

    private void enableBluetooth() {
        // Ask user to enable bluetooth if it is currently disabled
        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQ_BLUETOOTH_ENABLE);
    }
}