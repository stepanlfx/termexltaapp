package com.sgn.stepanlfx.termexltaapp;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;
import com.robotpajamas.blueteeth.BlueteethDevice;
import com.robotpajamas.blueteeth.BlueteethResponse;
import com.sgn.stepanlfx.termexltaapp.peripherals.SamplePeripheral;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.Objects;

import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ConnectionFragment.OnConnectionListener,ConsoleFragment.OnMessageSendListener {

    final int RECIEVE_MESSAGE = 1;					// Статус для Handler
    boolean isAnswerObtained = true;

    // переменные для оценки времени выполнения
    long start, finish;

    // фрагменты страниц приложения
    Fragment fragmentConnect, fragmentConsole;
    FragmentManager fragmentManager;

    private ConnectedThread mConnectedThread;
    // Текст лога
    final SpannableStringBuilder SpLog = new SpannableStringBuilder("");
    public StringBuilder sb = new StringBuilder();

    @Override
    public void onConnectionListener(BlueteethDevice sBlueteethDevice) {
        if (mConnectedThread!=null) {
            mConnectedThread.cancel();
            mConnectedThread=null;
        }
        else
        {
            // Создание информационного потока
            String message = "Попытка подключения\n";
            try {
                byte [] messageBytes = message.getBytes("cp1251");
                hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            mConnectedThread = new ConnectedThread(sBlueteethDevice);
            mConnectedThread.start();
        }
    }

    @Override
    public void onMessageSendListener(byte[] command) {
        if (mConnectedThread!=null)
            if (isAnswerObtained)
            {
                isAnswerObtained = false;
                start = System.currentTimeMillis();
                mConnectedThread.write(command);    // Отправляем через Bluetooth команду
            }
        else
            {
                finish = System.currentTimeMillis();
                long timeConsumedMillis = finish - start;
                if (timeConsumedMillis>3500) {
                    // Создание информационного потока
                    String message = "Превышено ожидание ответа на предыдущую команду\n";
                    try {
                        byte [] messageBytes = message.getBytes("cp1251");
                        hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    mConnectedThread.write(command);    // Отправляем через Bluetooth команду
                }
            }
        else Timber.d("Spam");
    }

    private static class SHandler extends Handler {
        final WeakReference<MainActivity> mActivity;

        SHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }
    }

    static SHandler hRead;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Создать новые фрагмент
        fragmentConnect = null;
        fragmentConsole = null;
        Class fragmentClass;

        fragmentClass = ConnectionFragment.class;

        try {
            fragmentConnect = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragmentClass = ConsoleFragment.class;

        try {
            fragmentConsole = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Вставить фрагмент
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.main_container, fragmentConnect, "Connection").commit();
        fragmentManager.beginTransaction().add(R.id.main_container, fragmentConsole, "Console").commit();
        fragmentManager.beginTransaction().hide(fragmentConsole).commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        //onNavigationItemSelected();

        ButterKnife.bind(this);

        hRead = new SHandler(this) {
            public void handleMessage(android.os.Message msg) {
                MainActivity activity = mActivity.get();
                if (activity != null)
                    switch (msg.what) {
                        case RECIEVE_MESSAGE:													// если приняли сообщение в Handler
                            byte[] readBuf = (byte[]) msg.obj;
                            String strIncom;
                            try
                            {
                                strIncom = new String(readBuf, 0, msg.arg1, "cp1251");

                                final ForegroundColorSpan style;
                                if (msg.arg2==1)
                                {
                                    SpLog.append(">");
                                    style = new ForegroundColorSpan(Color.BLUE);
                                }
                                else
                                {
                                    //SpLog.append("<");
                                    style = new ForegroundColorSpan(Color.BLACK);
                                }

                                SpLog.append(strIncom);  // формируем строку
                                sb.append(strIncom);

                                int indexCom = -1;
                                // парсинг строки
                                while ((indexCom=sb.indexOf("\n\u0004"))!=-1)
                                {
                                    // строка-ответ
                                    String dMessage = sb.substring(0, indexCom);
                                    sb.delete(0, indexCom+1);

                                    //finish = System.currentTimeMillis();
                                    //long timeConsumedMillis = finish - start;
                                    isAnswerObtained = true;
                                    // Создание информационного потока
                                    /*String message = Long.toString(timeConsumedMillis);
                                    try {
                                        byte [] messageBytes = message.getBytes("cp1251");
                                        hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }*/

                                    /*if ((dMessage.contains(""))||(dMessage.contains("ring")))
                                    {

                                    }*/
                                    /*else
                                        // ��� ������� ������ �������
                                        // ����� ������ ������ �������, ������ ������ "Qbat=***%"
                                        if ((indexCom=dMessage.indexOf("Qbat="))!=-1)
                                        {
                                            String bLevel = dMessage.substring(indexCom+5, dMessage.length());
                                            while (bLevel.indexOf(" ")!=-1)
                                                bLevel = bLevel.substring(1, bLevel.length());
                                            int bPercent = Integer.parseInt(bLevel);
                                            if (bPercent<20) txtBatteryLevel.setBackgroundResource(R.drawable.panel_bg_red);
                                            else
                                            if (bPercent<40) txtBatteryLevel.setBackgroundResource(R.drawable.panel_bg_orange);
                                            else
                                            if (bPercent<60) txtBatteryLevel.setBackgroundResource(R.drawable.panel_bg_yellow);
                                            else
                                            if (bPercent<80) txtBatteryLevel.setBackgroundResource(R.drawable.panel_bg_l_green);
                                            else
                                                txtBatteryLevel.setBackgroundResource(R.drawable.panel_bg_green);
                                            int wdt = txtBattery.getWidth();
                                            txtBatteryLevel.setWidth(wdt*bPercent/100);

                                            txtBattery.setText(Integer.toString(bPercent) + "%");
                                        }
                                        else
                                            // ��� ������� ���������� � �������
                                            if ((dMessage.indexOf("INFO")!=-1)&&(!ListeningInfo))
                                            {
                                                ListeningInfo=true;
                                                etMulti.setText("");
                                            }
                                            else
                                                // ����� ��������� ����������
                                                if ((dMessage.indexOf("INFO")!=-1)&&(dMessage.length()>7))
                                                {
                                                    String txtMessage = dMessage.substring(7);
                                                    etMulti.setText(etMulti.getText().toString()+txtMessage+"\n");
                                                    if (dMessage.indexOf("INFO31")!=-1)
                                                    {
                                                        ListeningInfo=false;
                                                    }
                                                }*/
                                }

                                // подключаем FragmentManager
                                FragmentManager fragmentManager = getSupportFragmentManager();

                                // Получаем ссылку на фрагмент по ID
                                ConsoleFragment conFragment = (ConsoleFragment) fragmentManager
                                        .findFragmentById(R.id.main_container);

                                if (SpLog.length()>1000) {
                                    int indexOfNString = SpLog.toString().indexOf("\n");
                                    SpLog.delete(0, indexOfNString + 1);
                                }

                                // Выводим нужную информацию
                                if (conFragment != null) {
                                    //SpLog.setSpan(style, conFragment.txtLog.getText().length(), SpLog.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                                    conFragment.txtLog.setText(SpLog);
                                }

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                    }
            }
        };
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case (R.id.nav_connection):
                {
                    fragmentManager.beginTransaction().hide(fragmentConsole).show(fragmentConnect).commit();
                break;
                }
            case (R.id.nav_console):
                {
                    fragmentManager.beginTransaction().hide(fragmentConnect).show(fragmentConsole).commit();
                break;
                }
            default:
                //fragmentClass = MainFragment.class;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

private class ConnectedThread extends Thread {

    private SamplePeripheral mSamplePeripheral;
    private boolean mIsConnected;

    boolean mIndicateEnabled = false;

    ConnectedThread(BlueteethDevice cBlueteethDevice) {
        String message = "Создание нового информационного потока\n";
        try {
            byte [] messageBytes = message.getBytes("cp1251");
            hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        mSamplePeripheral = new SamplePeripheral(cBlueteethDevice);
        mSamplePeripheral.connect(true, isConnected -> {mIsConnected = isConnected;});

        if (mIsConnected)
            message = "Поток создан\n";
        else
            message = "Создание потока прошло неудачно\n";
        try {
            byte [] messageBytes = message.getBytes("cp1251");
            hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        message = "Активания индикации\n";
        try {
            byte [] messageBytes = message.getBytes("cp1251");
            hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] activator = "".getBytes();
        mSamplePeripheral.writeEcho(activator, response -> {if (response != BlueteethResponse.NO_ERROR) { }});
        mIndicateEnabled = mSamplePeripheral.toggleIndication(mIndicateEnabled, (response, data) -> {if (response != BlueteethResponse.NO_ERROR) { }});

        if (mIndicateEnabled)
            message = "Индикация активирована\n";
        else
            message = "Активация индикации прошла неудачно\n";
        try {
            byte [] messageBytes = message.getBytes("cp1251");
            hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();		// Отправляем в очередь сообщений Handler
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        // Прослушивание входящего потока
        while (true) {
            // Чтение из входящего потока
            byte[] message;
            try {
                if (!mIndicateEnabled) {

                    mIndicateEnabled = mSamplePeripheral.toggleIndication(mIndicateEnabled, (response, data) -> {
                        if (response != BlueteethResponse.NO_ERROR) {
                        }
                    });
                    if (mIndicateEnabled) {
                        String logmessage = "Повторная активания индикации успешна\n";
                        try {
                            byte[] messageBytes = logmessage.getBytes("cp1251");
                            hRead.obtainMessage(RECIEVE_MESSAGE, messageBytes.length, 1, messageBytes).sendToTarget();        // Отправляем в очередь сообщений Handler
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                message = mSamplePeripheral.readEcho(/*(response, data) -> {if (response != BlueteethResponse.NO_ERROR) { }}*/).getBytes("cp1251");
                if (message.length>0) {
                    hRead.obtainMessage(RECIEVE_MESSAGE, message.length, 0, message).sendToTarget();        // Отправляем в очередь сообщений Handler
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /* Отправка данных удаленному устройству */
    void write(byte[] message) {
        mSamplePeripheral.writeEcho(message, response -> {if (response != BlueteethResponse.NO_ERROR) { }});
        hRead.obtainMessage(RECIEVE_MESSAGE, message.length, 1, message).sendToTarget();		// Отправляем в очередь сообщений Handler
    }

    /* Закрытие соединения */
    void cancel() {
        mSamplePeripheral.disconnect(isConnected -> {mIsConnected = isConnected;});
        mSamplePeripheral.close();
    }
}
}

