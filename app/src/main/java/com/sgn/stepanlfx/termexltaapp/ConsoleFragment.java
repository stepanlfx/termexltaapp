package com.sgn.stepanlfx.termexltaapp;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.io.UnsupportedEncodingException;

import static android.view.KeyEvent.KEYCODE_ENTER;

public class ConsoleFragment extends Fragment {

    Button btnSendCommand;
    TextView txtLog;
    EditText editCommandLine;

    public interface OnMessageSendListener {
        void onMessageSendListener(byte[] command);
    }

    private OnMessageSendListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMessageSendListener) {
            mListener = (OnMessageSendListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragment1DataListener");
        }
    }

    Typeface typeface = Typeface.MONOSPACE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ConsoleView =  inflater.inflate(R.layout.console, container, false);
        setRetainInstance(true);

        editCommandLine = ConsoleView.findViewById(R.id.editTextCommandLine);
        editCommandLine.setTypeface(typeface);
        txtLog = ConsoleView.findViewById(R.id.textViewLog);
        txtLog.setMovementMethod(new ScrollingMovementMethod());
        txtLog.setTypeface(typeface);
        btnSendCommand = ConsoleView.findViewById(R.id.buttonSendCommand);

        btnSendCommand.setOnClickListener(v -> {
                btnSendCommand.setEnabled(false);
                byte[] command;
                try {
                    String StrEnder;

                    // добавляем символ конца строки
                    byte[] ByteEnder = new byte[1];
                    ByteEnder[0] = 10;
                    StrEnder = new String(ByteEnder, "cp1251");

                    // отправляем команду
                    String comm = editCommandLine.getText().toString() + StrEnder;
                    command = comm.getBytes("cp1251");

                    if (!comm.isEmpty())
                    {
                        mListener.onMessageSendListener(command);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                btnSendCommand.setEnabled(true);
        });

        editCommandLine.setOnKeyListener((v, keyCode, event) -> {
            if (event.getKeyCode() == KEYCODE_ENTER)
                btnSendCommand.callOnClick();
            return false;
        });

        // очистка лога при долгом нажатии
        txtLog.setOnLongClickListener(v -> {
            txtLog.setText("");
            //SpLog.clear();
            return false;
        });

        return ConsoleView;
    }
}