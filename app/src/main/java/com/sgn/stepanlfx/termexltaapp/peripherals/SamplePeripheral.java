package com.sgn.stepanlfx.termexltaapp.peripherals;

import com.robotpajamas.blueteeth.BlueteethDevice;
import com.robotpajamas.blueteeth.BlueteethUtils;
import com.robotpajamas.blueteeth.listeners.OnCharacteristicReadListener;
import com.robotpajamas.blueteeth.listeners.OnCharacteristicWriteListener;

import java.util.UUID;

public class SamplePeripheral extends BaseBluetoothPeripheral {

    // Custom Service
    private static final UUID SERVICE_TEST = UUID.fromString("1D5688DE-866D-3AA4-EC46-A1BDDB37ECF6");
    private static final UUID CHARACTERISTIC_INDICATE = UUID.fromString("AF20FBAC-2518-4998-9AF7-AF42540731B3");
    private static final UUID CHARACTERISTIC_WRITE_ECHO = UUID.fromString("AF20FBAC-2518-4998-9AF7-AF42540731B3");

    public SamplePeripheral(BlueteethDevice device) {
        super(device);
    }

    public boolean toggleIndication(boolean isEnabled, OnCharacteristicReadListener readListener) {
        return mPeripheral.setIndication(CHARACTERISTIC_INDICATE, SERVICE_TEST, readListener, BlueteethDevice.Notifications.INDICATE);
    }

    public void writeEcho(byte[] dataToWrite, OnCharacteristicWriteListener writeListener) {
        BlueteethUtils.writeData(dataToWrite, CHARACTERISTIC_WRITE_ECHO, SERVICE_TEST, mPeripheral, writeListener);
    }

    public String readEcho(){//OnCharacteristicReadListener readListener) {
        //BlueteethUtils.read(CHARACTERISTIC_INDICATE, SERVICE_TEST, mPeripheral, readListener);
        String nMessage = mPeripheral.sb.toString();
        mPeripheral.sb.delete(0, mPeripheral.sb.length());
        return nMessage;
    }
}